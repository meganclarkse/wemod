<?php
namespace App\Entity;

use App\Repository\UrlsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UrlsRepository::class)]
class Urls
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'SEQUENCE')]
    #[ORM\SequenceGenerator(sequenceName: 'urls_id_seq')]
    #[ORM\Column(nullable: false)]
    private $id;

    #[ORM\Column]
    private ?string $short_url = null;

    #[ORM\Column]
    private ?string $long_url = null;

    #[ORM\Column(nullable: false)]
    private ?int $visit_count = 0;

    #[ORM\Column]
    private ?string $created = null;

    #[ORM\Column]
    private ?string $updated = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getShortUrl(): ?string
    {
        return $this->short_url;
    }

    public function setShortUrl($url)
    {
        $this->short_url = $url;
    }

    public function getLongUrl(): ?string
    {
        return $this->long_url;
    }

    public function setLongUrl($url)
    {
        $this->long_url = $url;
    }

    public function getVisitCount(): int
    {
        return $this->visit_count;
    }

    public function setVisitCount($visitCount)
    {
        $this->visit_count = $visitCount;
    }

    public function getCreated(): ?string
    {
        return $this->created;
    }

    public function setCreated()
    {
        $this->created = date('Y-m-d h:i:s');
    }

    public function getUpdated(): ?string
    {
        return $this->updated;
    }

    public function setUpdated()
    {
        $this->updated = date('Y-m-d h:i:s');
    }
}