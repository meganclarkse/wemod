<?php

namespace App\Controller;

use App\Entity\Urls;
use App\Form\Type\UrlsType;
use Exception;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UrlsController extends AbstractController
{
    #[Route('/metrics/{url}', name: 'get_url', methods: ['GET'])]
    public function getShortUrlMetrics(string $url, EntityManagerInterface $entityManager): JsonResponse
    {
        $urlsRepo = $entityManager->getRepository(Urls::class);
        $sanitizedUrl = $this->sanitizeUrl($url);

        $shortUrlResult = $urlsRepo->findOneBy([
            'short_url' => $sanitizedUrl
        ]);

        if($shortUrlResult) {
            return $this->json([
                'short_url' => $shortUrlResult->getShortUrl(),
                'long_url' => $shortUrlResult->getLongUrl(), 
                'visit_count' => $shortUrlResult->getVisitCount(),
                'created' => $shortUrlResult->getCreated(),
                'updated' => $shortUrlResult->getUpdated()
            ]);
        } else {
            return $this->json([
                'error' => 'Short url not found in DB.'
            ]);
        }
    }

    //API method for taking in a JSON body of URLs
    #[Route('/urls', name: 'save_urls', methods: ['POST'], format: 'json')]
    public function saveUrls(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        try {
            $payload = json_decode($request->getContent(), true);
            if($payload && !empty($payload['urls'])) {

                foreach($payload['urls'] as $url) {
                    $this->saveUrl($url, $entityManager);
                }

                return $this->json([
                    'message' => 'Urls received and saved!',
                ]);
            }
        } catch (\LogicException $e) {
            throw new \LogicException('Error encountered saving urls.');
        }
    }

    protected function saveUrl(string $url, EntityManagerInterface $entityManager)
    {
        //If full links were desired in the input data (eg https://www.test.com/asdfsadf)
        //would remove https//, extract content after first /, then sanitize
        try {
            $shortUrl = $this->generateHash($this->sanitizeUrl($url));

            $urlEntity = new Urls();
            $urlEntity->setShortUrl($shortUrl);
            $urlEntity->setLongUrl($this->sanitizeUrl($url));
            $urlEntity->setCreated();
            $urlEntity->setUpdated();
            
            $entityManager->persist($urlEntity);
            $entityManager->flush();
        } catch(Exception $e) {
            return false;
        }

        return true;
    }

    protected function sanitizeUrl(string $url) {
        $urlSantitized = trim($url, '/'); //trim preceeding and trailing /
        $urlSantitized = urlencode($urlSantitized);
        $urlSantitized = filter_var($urlSantitized, FILTER_SANITIZE_URL);
        
        return $urlSantitized;
    }

    //Takes in a CSV of urls. Shortens and uploads to DB
    #[Route('/uploadCsv', name: 'upload_urls_csv', methods: ['GET', 'POST'])]
    public function uploadCsv(Request $request, EntityManagerInterface $entityManager): Response
    {
        $urlsEntity = new Urls();
        $form = $this->createForm(UrlsType::class, $urlsEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $urls */
            $urlsFile = $form->get('urls_upload')->getData();

            if ($urlsFile) {
                $success = true;
                //Process urls from csv
                if (($handle = fopen($urlsFile->getPathname(), "r")) !== false) {
                    while (($data = fgetcsv($handle)) !== false) {
                        $success = $this->saveUrl($data[0], $entityManager);
                    }
                    fclose($handle);
                }

                if($success == false) {
                    //a url failed to save, could make logging a lot better
                }
            }
        }

        return $this->render('urls/upload.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/t/{url}', name: 'transform_url', methods: ['GET'])]
    public function transformUrl(string $url, EntityManagerInterface $entityManager)
    {
        $urlsRepo = $entityManager->getRepository(Urls::class);
        $sanitizedUrl = $this->sanitizeUrl($url);

        $shortUrlResult = $urlsRepo->findOneBy([
            'short_url' => $sanitizedUrl
        ]);

        if($shortUrlResult) {
            $shortUrlResult->setVisitCount($shortUrlResult->getVisitCount() + 1); //could do this better
            $entityManager->flush();

            return $this->redirectToRoute('transform_url', [
                'url' => $shortUrlResult->getLongUrl()
            ]);
        } else {
            //Check if this is a short_url, if so increment metric stats
            $longUrlResult = $urlsRepo->findOneBy([
                'long_url' => $sanitizedUrl
            ]);

            if($longUrlResult) {
                $shortUrl = $longUrlResult->getShortUrl();

                $longUrlResult->setVisitCount($longUrlResult->getVisitCount() + 1); //could do this better
                $entityManager->flush();
            } else {
                //this short url doesn't exist in the db, could saveUrl() here if wanted
            }
        }

        return $this->render('urls/index.html.twig', [
            'url' => $sanitizedUrl,
            'short_url' => $shortUrl ?? 'N/A'
        ]);
    }

    public function generateHash($longUrl) 
    {
        $fullHash = base64_encode(hash('sha256', $longUrl));
        $hash_urlsafe = strtr($fullHash, '+/', '-_');

        return substr($hash_urlsafe, 0, 10);
    }
}