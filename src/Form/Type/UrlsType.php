<?php
namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UrlsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        //Most basic options possible
        //Lots of room for enhancement with file type, size restrictions, styling
        $builder
            ->add('urls_upload', FileType::class, [
                'label' => 'Urls CSV',
                'mapped' => false,
                'required' => true
            ])
            ->add('save', SubmitType::class)
        ;
    }
}