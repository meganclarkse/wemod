CREATE SEQUENCE urls_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE TABLE urls (
    id INT PRIMARY KEY,
    short_url varchar(64) NOT NULL, 
    long_url varchar(255) NOT NULL,
    visit_count INT NOT NULL DEFAULT 0,
    created DATE DEFAULT NOW(),
    updated DATE DEFAULT NOW()
);

-- INSERT INTO urls values(1, 'testRecord', 'test23524623463264352345234523452346535', NOW(), NOW());

